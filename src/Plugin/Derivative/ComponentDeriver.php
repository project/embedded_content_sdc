<?php

namespace Drupal\embedded_content_sdc\Plugin\Derivative;


use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\sdc\ComponentPluginManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides block plugin definitions for custom menus.
 *
 * @see \Drupal\system\Plugin\Block\SystemMenuBlock
 */
class ComponentDeriver extends DeriverBase implements ContainerDeriverInterface {

  /**
   * @var \Drupal\sdc\ComponentPluginManager
   */
  protected $componentPluginManager;

  /**
   *
   */
  public function __construct(ComponentPluginManager $component_plugin_manager) {
    $this->componentPluginManager = $component_plugin_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $container->get('plugin.manager.sdc')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    foreach ($this->componentPluginManager->getAllComponents() as $component) {
      $this->derivatives[$component->getPluginId()] = $base_plugin_definition;
      $this->derivatives[$component->getPluginId()]['admin_label'] = $component->getPluginDefinition()['name'];
      $this->derivatives[$component->getPluginId()]['label'] = $component->getPluginDefinition()['name'];
    }
    return $this->derivatives;
  }

}
