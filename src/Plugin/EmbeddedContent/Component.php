<?php

namespace Drupal\embedded_content_sdc\Plugin\EmbeddedContent;

use Drupal\embedded_content\Annotation\EmbeddedContent;
use Drupal\embedded_content\EmbeddedContentInterface;
use Drupal\embedded_content\EmbeddedContentPluginBase;
use Drupal\cl_editorial\Form\ComponentInputToForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\sdc\ComponentPluginManager;
use SchemaForms\Drupal\FormGeneratorDrupal;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin iframes.
 *
 * @EmbeddedContent(
 *   id = "sdc",
 *   label = @Translation("Single Directory Components"),
 *   description = @Translation("Allows inserting components."),
 *   deriver = "\Drupal\embedded_content_sdc\Plugin\Derivative\ComponentDeriver",
 * )
 */
class Component extends EmbeddedContentPluginBase implements EmbeddedContentInterface, ContainerFactoryPluginInterface {

  /**
   * The component plugin manager.
   *
   * @var \Drupal\sdc\ComponentPluginManager
   */
  protected $componentManager;

  /**
   * The form generator from schema.
   *
   * @var \SchemaForms\Drupal\FormGeneratorDrupal
   */
  protected $formGenerator;

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static (
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.sdc'),
      $container->get('cl_editorial.form_generator')
    );
  }


  public function __construct(array $configuration, $plugin_id, $plugin_definition, ComponentPluginManager $component_manager, FormGeneratorDrupal $form_generator) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->componentManager = $component_manager;
    $this->formGenerator = $form_generator;
  }

  public function build(): array {
    $component = $this->getDerivativeId();
    return [];
  }

  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $component = $this->getDerivativeId();
    $form = cl_editorial_component_mappings_form($component, [], $form, $form_state);
    $input_to_form  = new ComponentInputToForm($this->componentManager, $this->formGenerator);
    $form += $input_to_form->buildForm($component, $form_state->getValues(), $form, $form_state);
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function isInline(): bool {
    return FALSE;
  }

}
